# SUPPL (lib)

An entire library of useful functions, including:|- syslog subsystem|- INI file random access subsystem:- secure string functions|- command line parser|- NLS information provider|Full description at:|http://www2.inf.fh-rhein-sieg.de/~skaise2a/ska/sources.html#suppl|*The pre-releases are currently compiled with Borland C v5.2 only.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## SUPPLS.LSM

<table>
<tr><td>title</td><td>SUPPL (lib)</td></tr>
<tr><td>version</td><td>pre-release v2.7 (rev A)</td></tr>
<tr><td>entered&nbsp;date</td><td>2000-05-07</td></tr>
<tr><td>description</td><td>An entire library of useful functions</td></tr>
<tr><td>summary</td><td>An entire library of useful functions, including:|- syslog subsystem|- INI file random access subsystem:- secure string functions|- command line parser|- NLS information provider|Full description at:|http://www2.inf.fh-rhein-sieg.de/~skaise2a/ska/sources.html#suppl|*The pre-releases are currently compiled with Borland C v5.2 only.</td></tr>
<tr><td>keywords</td><td>c, programming, library, supplemental</td></tr>
<tr><td>author</td><td>Steffen Kaiser &lt;Steffen.Kaiser _at_ FH-Rhein-Sieg.DE&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Steffen Kaiser &lt;Steffen.Kaiser _at_ FH-Rhein-Sieg.DE&gt;</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 2</td></tr>
</table>
