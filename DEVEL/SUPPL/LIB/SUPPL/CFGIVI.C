/*
    This file is part of SUPPL - the supplemental library for DOS
    Copyright (C) 1996-2000 Steffen Kaiser

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
/* $RCSfile: CFGIVI.C $
   $Locker: ska $	$Name:  $	$State: Exp $

	int cfgValInteger(const char * const name, int dflt);

	Return the data of the value of the specified name parsed as integer.
	If name == NULL, the currently scanned value is processed.

	Return:
		dflt: on error
		else: the parsed value

ob: cfgValInteger
su: inifile/2
sh: Return a value as integer
lo: Return the data of the specified value parsed as integer.\par
	If \para{name} == NULL, the current value is processed.
va: dflt: on error
	\item else: parsed value
re: 
fi: cfgivi.c
in:

*/

#include "initsupl.loc"

#ifndef _MICROC_
#endif
#include "inifile.loc"
#include "str.h"

#include "suppldbg.h"

#ifdef RCS_Version
static char const rcsid[] = 
	"$Id: CFGIVI.C 1.4 2001/02/27 01:27:47 ska Exp ska $";
#endif

int cfgValInteger(const char * const name, int const dflt)
{	int num;

	DBG_ENTER("cfgValInteger", Suppl_inifile2)
	DBG_ARGUMENTS( ("name=\"%s\", dflt=%d", name, dflt) )

	chkHeap
	DBG_RETURN_BI( cfgi_parseVal(name) == 0	/* fetch and decompose the specified value */
	 && strnum(I(data), &num, 0) == 0	/* parse number ASCII --> binary */
		?	num					/* success */
		:	dflt)				/* failure */
}
