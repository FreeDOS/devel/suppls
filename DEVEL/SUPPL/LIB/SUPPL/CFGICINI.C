/*
    This file is part of SUPPL - the supplemental library for DOS
    Copyright (C) 1996-2000 Steffen Kaiser

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
/* $RCSfile: CFGICINI.C $
   $Locker: ska $	$Name:  $	$State: Exp $

	int cfgCreateIntegerINI(char *name, int num)

	Create a signed numerical in its decimal ASCII represantation.
	The data is never "expandable".

ob(ject): cfgCreateIntegerINI
su(bsystem): inifile/1
sh(ort description):  Append a signed integer value to the INI file
lo(ng description): Append a signed integer value to the INI file currently opened for writing.
pr: \para{name} != NULL
re(lated to):
fi(le): cfgicini.c
in(itialized by): cfgCreateINI cfgCreateSectionINI cfgEraseSection

*/

#include "initsupl.loc"

#ifndef _MICROC_
#endif
#include "inifile.loc"
#include "str.h"

#include "suppldbg.h"

#ifdef RCS_Version
static char const rcsid[] = 
	"$Id: CFGICINI.C 1.4 1999/12/13 02:22:18 ska Exp ska $";
#endif

int cfgCreateIntegerINI(const char * const name, int num)
{	char buf[sizeof(int) * 8 / 3 + 3];	/* this is octal, so a decimal
		value will always fit into this buffer */

	DBG_ENTER("cfgCreateIntegerINI", Suppl_inifile1)
	DBG_ARGUMENTS( ("name=\"%s\", num=%d", name, num) )

	chkHeap
	itoa10(num, buf);

	chkHeap
	DBG_RETURN_BI( cfgi_putValue(name, CFG_TINTEGER, buf))
}
