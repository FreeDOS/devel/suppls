/*
    This file is part of SUPPL - the supplemental library for DOS
    Copyright (C) 1996-2000 Steffen Kaiser

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
/* $RCSfile: CFG_AIDX.C $
   $Locker: ska $	$Name:  $	$State: Exp $

ob(ject): cfgGetargIdx
su(bsystem): cmdline
ty(pe): 
sy(nopsis): 
sh(ort description): Return a specific argument
he(ader files): 
lo(ng description): Return a specific argument of the already cached ones
	into the output stack. The argument is identified with its index.
	This function does not work correctly
	if no arguments are cached at all and if
	the arguments are removed when read sequentially.
pr(erequistes): 
va(lue): NULL: on failure \item else: pointer to a static buffer containing the argument
re(lated to): 
se(condary subsystems): 
xr(ef): 
im(port): 
fi(le): cfg_aidx.c
in(itialized by): 
wa(rning): 
bu(gs): 

*/

#include "initsupl.loc"

#ifndef _MICROC_
#endif
#include <portable.h>
#include "supplio.h"
#include "cfg.loc"

#include "suppldbg.h"

#ifdef RCS_Version
static char const rcsid[] = 
	"$Id: CFG_AIDX.C 1.6 2001/02/27 01:28:00 ska Exp ska $";
#endif

struct fetchBuf {
	int idx;
	char *str;
};

static int fetch(struct Cfg_Getopt * const optstru
	, struct Cfg_oStackGetopt * const ostk, void * const arg)
{
#define Xbuf (cS(struct fetchBuf *)arg)
	assert(optstru);
	assert(ostk);
	assert(arg);

	if(Xbuf->idx < O(cnt)) {		/* found the item with the entry
										we look for */
		Xbuf->str = (O(rdArg))(optstru, ostk, Xbuf->idx);
		chkHeap;
		return 1;					/* found flag --> stop enumeration */
	}

	/* not in this item */
	Xbuf->idx -= O(cnt);
	chkHeap;
	return 0;					/* continue enumeration */
}

char *cfgGetargIdx(struct Cfg_Getopt * const optstru, int idx)
{	struct fetchBuf fb;

	DBG_ENTER("cfgGetargIdx", Suppl_cmdline)
	DBG_ARGUMENTS( ("idx=%d", idx) )

	assert(optstru);

	fb.idx = idx;
	fb.str = 0;		/* default return value --> error */
	cfg_ostkEnum(optstru, aF(fetch), aS(fb));

	DBG_RETURN_S( fb.str)		/* was modified by fetch() */
}
