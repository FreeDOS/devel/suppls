/*
    This file is part of SUPPL - the supplemental library for DOS
    Copyright (C) 1996-2000 Steffen Kaiser

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
/* $RCSfile: CFGARGI.C $
   $Locker: ska $	$Name:  $	$State: Exp $

	int cfgArgInteger(Cfg_Getopt * const optstru)

	Return the current argument in its native integer format.
	Must not be called unless optArgType() returned CFG_TINTEGER.

	The return value may be interpreted signed or unsigned, there is
	no way to tell which was originally specified.

	Return:
		if called when optArgType() != CFG_TINTEGER: undefined
		else: the integer value

ob(ject): cfgArgInteger
su(bsystem): cmdline
sh(ort description): Return the current argument as integer number
lo(ng description): Returns the current argument in its native integer
 numerical format.\par
 The number will be interpreted signed or unsigned, there is no way to tell
 which format was originally specified.
pr(erequistes): \item optstru != NULL \item optArgType() == CFG_TINTEGER
va(lue):  \em{undefined}: if called with optArgType() != CFG_TINTEGER
	\item else: integer value
re(lated to): cfgGetopt cfgGetoptInit cfgArgBoolean
se(condary subsystems): inifile
fi(le): cfgargi.c
in(itialized by): cfgGetoptInit cfgGetopt

*/

#include "initsupl.loc"

#ifndef _MICROC_
#endif
#include "cfg.loc"

#include "suppldbg.h"

#ifdef RCS_Version
static char const rcsid[] = 
	"$Id: CFGARGI.C 1.3 1999/12/13 02:22:08 ska Exp ska $";
#endif

int cfgArgInteger(struct Cfg_Getopt * const optstru)
{	DBG_ENTER("cfgArgInteger", Suppl_cmdline)
	assert(optstru);
	assert(optArgType() == CFG_TINTEGER);
	assert(S(argIntegerFct));

	DBG_RETURN_BI((S(argIntegerFct))(optstru))
}
