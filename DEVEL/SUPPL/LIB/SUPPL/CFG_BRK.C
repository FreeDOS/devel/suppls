/*
    This file is part of SUPPL - the supplemental library for DOS
    Copyright (C) 1996-2000 Steffen Kaiser

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
/* $RCSfile: CFG_BRK.C $
   $Locker: ska $	$Name:  $	$State: Exp $

ob(ject): cfgSplitBuffer
su(bsystem): cmdline
ty(pe): H
sy(nopsis): 
sh(ort description): Split the current input buffer at a specified address
he(ader files): 
lo(ng description): The current input buffer is splitted into a non-option
	argument and options. The options are the trailing part behind the
	specified address, no option character need to be specified.\par
	The string behind the specified address must remain unchanged until
	its arguments have been returned via \tok{cfgGetopt()} and \tok{cfgGetarg()}. It is pushed via \tok{cfgPushString()}. Otherwise use the
	\tok{cfgPushDynString(opstru, Estrdup(p))} function instead.\par
	If the specified address itself points to a \tok{'\0'} character, this
	function performs no action.\par
	The character at \tok{*p} is overwritten by \tok{'\0'}.
pr(erequistes): 
va(lue): none
re(lated to): 
se(condary subsystems): 
xr(ef): 
im(port): 
fi(le): cfg_brk.c
in(itialized by): 
wa(rning): 
bu(gs): 

*/

#include "initsupl.loc"

#ifndef _MICROC_
#include <string.h>
#endif
#include <portable.h>
#include "cfg.loc"

#include "suppldbg.h"

#ifdef RCS_Version
static char const rcsid[] = 
	"$Id: CFG_BRK.C 1.5 1999/12/13 02:21:57 ska Exp ska $";
#endif

void cfgSplitBuffer(struct Cfg_Getopt * const optstru, char * const p)
{	DBG_ENTER("cfgSplitBuffer", Suppl_cmdline)

	assert(optstru);
	assert(p);

	if(*p && p[1]) {
		chkHeap;
		cfgPushString(optstru, p + 1);

	 	S(newopt) = 1;		/* because no option character will be
	 								visible */
		*p = '\0';		/* split the buffer */
		chkHeap;
	}

	DBG_EXIT
}
