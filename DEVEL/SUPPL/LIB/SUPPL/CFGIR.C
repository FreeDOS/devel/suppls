/*
    This file is part of SUPPL - the supplemental library for DOS
    Copyright (C) 1996-2000 Steffen Kaiser

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
/* $RCSfile: CFGIR.C $
   $Locker: ska $	$Name:  $	$State: Exp $

	int cfgReset(void);

	If currently a write is pending, these changes are lost.
	Re-opens all INI files.

ob(ject): cfgReset
su(bsystem): inifile/2
sh(ort description): Reset the current state
lo(ng description): Throw away all pending changes, then close and re-open
	all INI files. On success the section \tok{NULL} is opened.
re(lated to): 
fi(le): cfgir.c
in(itialized by): cfgInitINI

*/

#include "initsupl.loc"

#ifndef _MICROC_
#endif
#include "inifile.loc"
#include "supplio.h"

#include "suppldbg.h"

#ifdef RCS_Version
static char const rcsid[] = 
	"$Id: CFGIR.C 1.4 2001/02/27 01:27:45 ska Exp ska $";
#endif

int cfgReset(void)
{	char *p;

	DBG_ENTER("cfgReset", Suppl_inifile2)

	chkHeap
	/* ignore any pending output */
	if(I(writeOpen))
		cfgi_wrClose(CFG_ERR_WRITE);
	chkHeap

	/* re-open any opened file */
	if((p = sysini.cfgi_fnam) != 0) {
		sysini.cfgi_fnam = 0;
		chkHeap
		Fyclose(sysini.cfgi_fp);
		chkHeap
		cfgi_opFileStru(aS(sysini), p);
		chkHeap
	}
	if((p = usrini.cfgi_fnam) != 0) {
		usrini.cfgi_fnam = 0;
		chkHeap
		Fyclose(usrini.cfgi_fp);
		chkHeap
		cfgi_opFileStru(aS(usrini), p);
		chkHeap
	}

	/* re-open section */
	DBG_RETURN_BI( cfgOpenSection(0))
}
