/*
    This file is part of SUPPL - the supplemental library for DOS
    Copyright (C) 1996-2000 Steffen Kaiser

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
/* $RCSfile: CFG_RDAQ.C $
   $Locker: ska $	$Name:  $	$State: Exp $

	Cfg_FctGetopt cfg_rdArgvQ

	Scan argv array considering that this array already has honored
	quotes

ob(ject): cfg_rdArgvQ
su(bsystem): cmdline
ty(pe): L
sy(nopsis): 
sh(ort description): Read from an argv-array skip quotes
he(ader files): 
lo(ng description): Read one character from an argv-array. It is assumed
	that quotes have been processed already.
pr(erequistes): 
va(lue): EOF: at end of array \item else: the character
re(lated to): 
se(condary subsystems): 
xr(ef): 
im(port): 
fi(le): cfg_rdaq.c
in(itialized by): 
wa(rning): 
bu(gs): 

*/

#include "initsupl.loc"

#ifndef _MICROC_
#endif
#include <ctype.h>
#include <portable.h>
#include "dynstr.h"
#include "cfg.loc"

#include "suppldbg.h"

#ifdef RCS_Version
static char const rcsid[] = 
	"$Id: CFG_RDAQ.C 1.5 2001/02/27 01:28:07 ska Exp ska $";
#endif

int cfg_rdArgvQ(struct Cfg_Getopt *optstru)
{	struct Cfg_ArgArgv *p;
	int ch;

	DBG_ENTER("cfg_rdArgvQ", Suppl_cmdline)

	assert(optstru);
	assert(C(_stk));

	chkHeap
	if((p = S(getArg)) != 0) {
		assert(p->cfg_a_argv);
		if(*p->cfg_a_argv) {		/* not end reached */
			if(!p->cfg_a_p) {		/* skip to next argument */
				if((p->cfg_a_p = *++p->cfg_a_argv) == 0)
					DBG_RETURN_C( EOF)		/* end of input stream reached */
				p->cfg_a_quote = 0;	/* no pending quote */
			}

			chkHeap
			ch = *p->cfg_a_p;
			if(!p->cfg_a_quote) {	/* check if to be quoted */
				if(isQuote(ch) || isspace(ch) || isSingleQuote(ch)) {
				/* needs to be quoted */
					p->cfg_a_quote = 1;
					DBG_RETURN_C( squoteSign)
				}
			}
			else p->cfg_a_quote = 0;

			if(!ch) {			/* argument finished */
				p->cfg_a_p = 0;	/* advance to next argument next time */
				DBG_RETURN_C( ' ')		/* return argument delimiter */
			}

			++p->cfg_a_p;
			DBG_RETURN_C( ch)
		}
	}

	chkHeap
	DBG_RETURN_C( EOF)
}
