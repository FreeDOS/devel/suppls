/*
    This file is part of SUPPL - the supplemental library for DOS
    Copyright (C) 1996-2000 Steffen Kaiser

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
/* $RCSfile: CFG_AATL.C $
   $Locker: ska $	$Name:  $	$State: Exp $

ob(ject): cfg_addArgToList
su(bsystem): cmdline
ty(pe): L
sy(nopsis): 
sh(ort description): add one argument to the output stack
he(ader files): 
lo(ng description): Adds the current argument to the current output
	stack using the current "write argument" function; to be used as
	as a "_foundArg()" function.\newline
	Additionally this function returns \tok{0} (zero) to indicate to
	\tok{cfgGetopt} that the found argument can be ignored so far.
pr(erequistes): 
va(lue): 0: on success
re(lated to): cfg_getStream cfgGetopt
se(condary subsystems): 
xr(ef): 
im(port): 
fi(le): cfg_aatl.c
in(itialized by): 
wa(rning): When there is no "write argument" function, meaning
	\tok{curOStk->C_wrArg == NULL}, the argument is ignored (== lost).
bu(gs): 

*/

#include "initsupl.loc"

#ifndef _MICROC_
#include <string.h>
#endif
#include <portable.h>
#include "cfg.loc"

#include "suppldbg.h"

#ifdef RCS_Version
static char const rcsid[] = 
	"$Id: CFG_AATL.C 1.5 1999/12/13 02:21:55 ska Exp ska $";
#endif

int cfg_addArgToList(struct Cfg_Getopt * const optstru)
{	DBG_ENTER("cfg_addArgToList", Suppl_cmdline)

	assert(optstru);
	assert(C(_stk));

	if(!C(_oHead))	DBG_RETURN_I( 1)		/* no output channel */

	chkHeap;

	if(curOStk->C_wrArg)
		(curOStk->C_wrArg)(0, optstru, curOStk, S(buf), strlen(S(buf)) + 1);

	DBG_RETURN_I( 0)
}
