/*
    This file is part of SUPPL - the supplemental library for DOS
    Copyright (C) 1996-2000 Steffen Kaiser

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
/* $RCSfile: CFGI_AN.C $
   $Locker: ska $	$Name:  $	$State: Exp $

	char *cfgi_appName(void)

	Return the name of the current application.

	Return always != NULL

ob: cfgi_appName
ty: L
su: inifile
sh: Return the name of the current application
va: always != NULL
re: cfgi_appname cfgChangeNameINI
fi: cfgi_an.c
in:

*/

#include "initsupl.loc"

#ifndef _MICROC_
#include <stdlib.h>
#endif
#include "inifile.loc"
#include "appName.h"

#include "suppldbg.h"

#ifdef RCS_Version
static char const rcsid[] = 
	"$Id: CFGI_AN.C 1.4 2001/02/27 01:27:47 ska Exp ska $";
#endif

char *cfgi_appName(void)
{	char *p;

	DBG_ENTER("cfgi_appName", Suppl_inifile)

	chkHeap

	if((p = cfgi_appname) == 0 && (p = appNameEx()) == 0)
		DBG_RETURN_S( ".CFG")

	chkHeap
	DBG_RETURN_S( p)
}
