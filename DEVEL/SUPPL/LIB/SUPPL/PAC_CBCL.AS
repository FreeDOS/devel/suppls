;	Pacific C ^Break catcher 
;	Large code & large data version
; See cb_catch.asm for more details

	.globl	large_data
	.globl	large_code
	.psect	ltext,local,class=CODE,reloc=16,size=65535
	.globl	__fct_cbreak
	.globl	__cbreak_catcher_ll
	.globl	_exit

__cbreak_catcher_ll:
		push bx
		push cx
		push dx
		push di
		push si
		push ds
		push es
		push bp
		push ax
		mov ax, #seg(__fct_cbreak)
		mov ds, ax
		mov ax, __fct_cbreak
		or ax, __fct_cbreak+2
		je cbreak1_catcher
		callf [__fct_cbreak]
		or ax, ax			; /* continue with program? */
		jne cbreak1_catcher
		; /* terminate the program, that's a bit of a problem, because */
		; /* MS built in some interesting stuff to make their DOS */
		; /* incompatible with DR DOS and Novell DOS. Therefore we don't */
		; /* let DOS terminate our program, but we terminate it ourselves */
		; /* with the exit code 3. That won't inform the caller, that */
		; /* we terminated because of ^C and probably leads to misinterpretation */
		; /* of the exit code. */
		; /* To let DOS terminate the program, change: */
		; /*	 1) in this branch: only set the Carry flag (stc). */
		; /*	 2) Replace the iret by retf (NOT retf 2!). */

		mov dx, #3
		callf _exit

cbreak1_catcher:
		pop ax
		pop bp
		pop es
		pop ds
		pop si
		pop di
		pop dx
		pop cx
		pop bx
		iret				; /* continue with the program */
	.end

ob: _cbreak_catcher_ll
su(bsystem): portable
sy: void interrupt _cbreak_catcher_ll(void)
im: _fct_cbreak
ty(pe): L
sh(ort description): ^Break catcher
lo(ng description): Low-level ^Break catcher for large-code & large-data
	memory models. See cb_break.asm for more information.
pr(erequistes): 
va(lue): none
re(lated to): ctrlbrk
se(condary subsystems): 
bu(gs): 
co(mpilers): Pacific HiTech C only

