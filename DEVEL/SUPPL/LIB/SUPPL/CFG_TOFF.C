/*
    This file is part of SUPPL - the supplemental library for DOS
    Copyright (C) 1996-2000 Steffen Kaiser

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
/* $RCSfile: CFG_TOFF.C $
   $Locker: ska $	$Name:  $	$State: Exp $

ob(ject): cfgNamesOff
su(bsystem): cmdline
ty(pe): 
sh(ort description): Enumerates the strings for "false boolean option"
lo(ng description): 
pr(erequistes): 
va(lue): for the English NLS defaults to: "off", "false", "no",
	"clear", and the minus sign
re(lated to): 
se(condary subsystems): nls
in(itialized by): static
wa(rning): 
bu(gs): 
co(mpilers): 

*/

#include "initsupl.loc"

#ifndef _MICROC_
#endif
#include <portable.h>
#include "msgs.loc"
#include "cfg.loc"

#include "suppldbg.h"

#ifdef RCS_Version
static char const rcsid[] = 
	"$Id: CFG_TOFF.C 1.4 1999/12/13 02:22:07 ska Exp ska $";
#endif

const MSGID cfgNamesOff[] = {
	iM(MSGID) I_wdOff, I_wdFalse, I_wdNo, I_wdClear, I_sgnMinus, (MSGID)0
};
