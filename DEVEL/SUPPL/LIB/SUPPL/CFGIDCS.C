/*
    This file is part of SUPPL - the supplemental library for DOS
    Copyright (C) 1996-2000 Steffen Kaiser

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
/* $RCSfile: CFGIDCS.C $
   $Locker: ska $	$Name:  $	$State: Exp $

	int cfgDeleteCurSection(void);

	Delete current section.

ob(ject): cfgDeleteCurSection
su(bsystem): inifile/2
sh(ort description): Delete the current section
lo(ng description): Delete the current section from the INI file.
 On success, the NULL section is opened.
re(lated to):
fi(le): cfgidcs.c
in(itialized by):

*/

#include "initsupl.loc"

#ifndef _MICROC_
#endif
#include "inifile.loc"
#include "supplio.h"
#include "dynstr.h"

#include "suppldbg.h"

#ifdef RCS_Version
static char const rcsid[] = 
	"$Id: CFGIDCS.C 1.3 1999/12/13 02:22:20 ska Exp ska $";
#endif

int cfgDeleteCurSection(void)
{	int rv;

	DBG_ENTER("cfgDeleteCurSection", Suppl_inifile2)

	chkHeap
	if(!hasKey(usrini))	/* no file --> no section --> is deleted */
		DBG_RETURN_I( CFG_ERR_NONE)

	chkHeap
	rv = cfgi_wrSecOpen(aS(usrini.cfgi_keypos), 1);
	chkHeap

	DBG_RETURN_I( cfgi_wrClose(rv))
}
