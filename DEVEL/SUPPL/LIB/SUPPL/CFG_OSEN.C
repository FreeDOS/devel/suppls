/*
    This file is part of SUPPL - the supplemental library for DOS
    Copyright (C) 1996-2000 Steffen Kaiser

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
/* $RCSfile: CFG_OSEN.C $
   $Locker: ska $	$Name:  $	$State: Exp $

ob(ject): cfg_ostkEnum
su(bsystem): cmdline
ty(pe): L
sy(nopsis): 
sh(ort description): Invoke a function for all output stack items
he(ader files): 
lo(ng description): Invoke a function for all output stack items.\par
	The callback function must have the type \tok{Cfg_ostkEnumFct}.\newline
	The argument \para{arg} is passed unchanged to the callback function.\par
	The specified callback function must return \tok{0} (zero) to
	continue enumeration.
pr(erequistes): 
va(lue): 0: if the callback function always returned \tok{0}
	\item else: the value returned by the callback function
re(lated to): 
se(condary subsystems): 
xr(ef): 
im(port): 
fi(le): cfg_osen.c
in(itialized by): 
wa(rning): 
bu(gs): 

*/

#include "initsupl.loc"

#ifndef _MICROC_
#endif
#include <portable.h>
#include "cfg.loc"

#include "suppldbg.h"

#ifdef RCS_Version
static char const rcsid[] = 
	"$Id: CFG_OSEN.C 1.5 2001/02/27 01:28:04 ska Exp ska $";
#endif

int cfg_ostkEnum(struct Cfg_Getopt * const optstru,
	Cfg_ostkEnumFct fct, void *arg)
{	struct Cfg_oStackGetopt *p;
	int rv;

	DBG_ENTER("cfg_ostkEnum", Suppl_cmdline)

	assert(optstru);
	assert(fct);

	if((p = C(_oHead)) == 0)
		DBG_RETURN_I(0)

	while((rv = (fct)(optstru, p, arg)) == 0
	 && (p = p->C_nxt) != C(_oHead));

	DBG_RETURN_I( rv)
}
