Just go into the most appropriate subdirectory of your preferred compiler
located in sub-directory COMPILER:

BORLANDC	Borland C/C++ v2+
mc314		Micro-C v3.14
mc315		Micro-C v3.15
mc315		Micro-C v3.16
PAC751		HiTech Pacific C v7.51
tc101		Turbo C++ v1.01
turboc		Turbo C++ v3
wc106		Watcom v10.6

However: The compiler I develop the library with is Borland C v5.2, for all the
other compilers I prefer the term "SUPPL compiles under ..." instead of
"Works with ...". [I hope I get many bug reports/success stories in order to
change the term for the compilers. :) ]

Note: The included BIN\LIB.EXE program archives OBJ/LIB compatible
files. It always chooses the smallest possible page size. However,
some tools seem to have problems using page sizes, which are not
a power of two; e.g. Borland's TDUMP and TLIB fall into this category,
but their TLINK does not.
To change the page size run:
	LIB /CP=16 SUPPL_S.LIB
(a page size of 16 should be large enough).

If you cannot use this librarian and the librarian shipped with your compiler
does not support wildcards, use the following template to add the
object files one by one:

.\foreach compile.ccc <librarian_and_options> SUPPL_#.LIB %%fname:.OBJ

- replace '#' by the memory model,
- 'LIB' by the name archives usually have with your compiler,
- 'OBJ' by the name compiled files usually have with your compiler, &
- '<librarian_and_options>' by the string to add one file to a library.

See the documentation of your compiler tools to learn more about the
options and arguments you need.
