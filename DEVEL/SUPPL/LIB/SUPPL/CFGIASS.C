/*
    This file is part of SUPPL - the supplemental library for DOS
    Copyright (C) 1996-2000 Steffen Kaiser

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
/* $RCSfile: CFGIASS.C $
   $Locker: ska $	$Name:  $	$State: Exp $

	int cfgASString(Cfg_addSetFct fct, const char * const name
	 , const char * const string, const char * const comment);

	Add/Set a quoted string value.

ob(ject): cfgASString
ty: L
su(bsystem): inifile/3
sh(ort description): Invoke a add/set INI value function for quoted string values
lo(ng description): Calls a function to modify a quoted string value of an INI file.
re(lated to): cfgAddString cfgSetString
fi(le): cfgiass.c

*/

#include "initsupl.loc"

#ifndef _MICROC_
#endif
#include "inifile.loc"

#include "suppldbg.h"

#ifdef RCS_Version
static char const rcsid[] = 
	"$Id: CFGIASS.C 1.3 1999/12/13 02:22:16 ska Exp ska $";
#endif

int cfgASString(Cfg_addSetFct fct, const char * const name
 	, const char * const string, const char * const comment)
{	DBG_ENTER("cfgASString", Suppl_inifile3)
	DBG_ARGUMENTS( ("name=\"%s\", str=\"%s\", cmt=\"%s\"", name, string, comment) )
	DBG_RETURN_BI((fct)(name, cfgi_quoteString(string), comment
		, CFG_TSTRING, 0))
}
