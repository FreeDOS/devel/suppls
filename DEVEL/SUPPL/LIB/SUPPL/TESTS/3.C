/*
    This file is part of SUPPL - the supplemental library for DOS
    Copyright (C) 1996-2000 Steffen Kaiser

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
/* Test of dfnexpand() */
#include <stdio.h>
#ifndef _MICROC_
#include <stdlib.h>
#include <string.h>
#include <dos.h>
#endif
#define SUPPL_DBG_HEAP
#define SUPPL_LOG_MEMORY
#define SUPPL_LOG_FUNCTION
#include "suppldbg.h"

#include "environ.h"
#include "dfn.h"
#include "mcb.h"

#define VAR "SK"
#define VAL1 "new variable"
#define VAL2 "change contents"

void hlpScreen()
{	exit(127);
}

int dispEnv(void *arg, word env, word offset)
{	unsigned char far *p;
	int *cnt;

	cnt = arg;
	p = MK_FP(env, offset);
	printf("%3u: ", ++*cnt);
	while(*p)
		putchar(*p++);
	putchar('\n');
	return 0;
}

main(int argc, char **argv)
{	int segm;
	int cnt;
	char *p;

	/** The DBG_ENTER() macro is used in its broken up format
		in order to have the opening of "main" to be logged **/
	DBG_ENTER1
		/* log everything incl. SUPPL library */
	DBG_CHANGE_STATE("l+;C+,+SUPPL")
	DBG_ENTER2("main", "usr")
	openlog("T", 0, 0);					/* memory logger */

	chkHeap

	printf("dfnexpand(\\p\\) = \"%s\"\n", dfnexpand("\\p\\", NULL));
	printf("dfnexpand(\\p\\.) = \"%s\"\n", dfnexpand("\\p\\.", NULL));
	printf("dfnexpand(\\p\\..) = \"%s\"\n", dfnexpand("\\p\\..", NULL));
	printf("dfnexpand(\\p\\...) = \"%s\"\n", dfnexpand("\\p\\...", NULL));

	DBG_RETURN_I(0)
}
