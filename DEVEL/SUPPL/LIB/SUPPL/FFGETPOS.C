/*
    This file is part of SUPPL - the supplemental library for DOS
    Copyright (C) 1996-2000 Steffen Kaiser

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
/* $RCSfile: FFGETPOS.C $
   $Locker: ska $	$Name:  $	$State: Exp $


ob(ject): Fgetpos
su(bsystem): supplio
ty(pe): 
sh(ort description): Get stream position
pr(erequistes): 
va(lue): \tok{0}: on success
	\item else: on failure
re(lated to): Fsetpos
se(condary subsystems): 
in(itialized by): 
wa(rning): 
bu(gs): 
co(mpilers): Micro-C only

*/

#include "initsupl.loc"

#ifdef _MICROC_
#include <stdio.h>
#include <portable.h>
#include "supplio.h"

#include "suppldbg.h"

#ifdef RCS_Version
static char const rcsid[] = 
	"$Id: FFGETPOS.C 1.5 2000/03/31 09:09:19 ska Exp ska $";
#endif

int Fgetpos(FILE *fp, fpos_t *pos)
{	return ftell(fp, &pos->hi, &pos->lo);
}

#endif
