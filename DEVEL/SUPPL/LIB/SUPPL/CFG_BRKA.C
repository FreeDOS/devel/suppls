/*
    This file is part of SUPPL - the supplemental library for DOS
    Copyright (C) 1996-2000 Steffen Kaiser

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
/* $RCSfile: CFG_BRKA.C $
   $Locker: ska $	$Name:  $	$State: Exp $

ob(ject): cfgSplitArg
su(bsystem): cmdline
ty(pe): 
sy(nopsis): 
sh(ort description): Split an argument from attached options
he(ader files): 
lo(ng description): Separates options attached at an argument. The original
	buffer must remain unchanged until the detached options has been
	aquired via \tok{cfgGetopt()} or \tok{cfgGetarg()}.
pr(erequistes): 
va(lue): none
re(lated to): 
se(condary subsystems): 
xr(ef): 
im(port): 
fi(le): cfg_brka.c
in(itialized by): 
wa(rning): 
bu(gs): 

*/

#include "initsupl.loc"

#ifndef _MICROC_
#include <string.h>
#endif
#include <portable.h>
#include "cfg.loc"
#include "sstr.h"

#include "suppldbg.h"

#ifdef RCS_Version
static char const rcsid[] = 
	"$Id: CFG_BRKA.C 1.6 2001/02/27 01:28:01 ska Exp ska $";
#endif

void cfgSplitArg(struct Cfg_Getopt * const optstru
 , char * const buf, const char * const optchars)
{	char *p;

	DBG_ENTER("cfgSplitArg", Suppl_cmdline)
	DBG_ARGUMENTS( ("arg=\"%s\", optChars=\"%s\"", buf, optchars) )

	assert(optstru);
	assert(C(_stk));

	if((p = Strpbrk(buf, optchars)) != 0)
	 	cfgSplitBuffer(optstru, p);

	DBG_EXIT
}
