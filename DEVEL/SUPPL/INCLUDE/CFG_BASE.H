/*
    This file is part of SUPPL - the supplemental library for DOS
    Copyright (C) 1996-2000 Steffen Kaiser

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
/* $RCSfile: CFG_BASE.H $
   $Locker: ska $	$Name:  $	$State: Exp $

	Declarations shared by INIFILE.H and CFG.H

*/

#ifndef __CFG_BASE_H
#define __CFG_BASE_H

/*
 *	What types the data of a value may have
 */
#define CFG_TNONE		0	/* type: none specified (must be zero) */
#define CFG_TSTRING		1		/* type: string */
#define CFG_TEXPAND		0x81	/* type: expandable string */
#define CFG_TBOOLEAN	3		/* type: boolean */
#define CFG_TINTEGER	5		/* type: signed integer number */
#define CFG_TERROR		0x7f	/* error, no source for type */

#define CFG_TEXPSTRING	(CFG_TSTRING | CFG_TEXPAND)
#define CFG_TEXPBOOLEAN	(CFG_TBOOLEAN | CFG_TEXPAND)
#define CFG_TEXPINTEGER	(CFG_TINTEGER | CFG_TEXPAND)

#define CFG_TDEFAULT	(-1)		/* use I(type) */
#define CFG_TUNKNOWN	CFG_TNONE

#define CFG_TBASE(type)	((type) & 0x7f)
#define CFG_TISEXPAND(type)	((type) & 0x80)

/*
 *	Compare two names of values / sections / keys / longname options
 *
 *	Note: Because longname options and value names of configuration
 *	files are the same, this function is also used to compare longname
 *	options.
 *
 *	Return:
 *		 <0: s1 < s2
 *		==0: s1 == s2
 *		 >0: s1 > s2
 */
int cfgCmpName(const char * const s1, const char * const s2);
#define cfgEqName(s1,s2)	(cfgCmpName((s1), (s2)) == 0)

#endif
